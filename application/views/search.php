<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<div class="container py-7 pt-5">
    <div class="row pt-6">
        <div class="col-12 mb-5">
            <h1><?php if($count>0): echo $count; else: echo "0"; endif; ?>&nbsp;Search results for "<?php echo $keyword ?>"</h1>
            <?php $this->load->view('components/common/pagination'); ?>
        </div>
        <div id="searchresults" class="col-12 px-5">
        <?php $i=0;foreach($postData as $result): ?>
                <div class="card result mb-2" >
                    <div class="card-body">
                        <a href="<?php echo base_url(); ?>viewpost/<?php echo $result->id; ?>"><b><?php echo $result->title; ?></b></a><br>
                        <div class="row">
                            <div class="col-6"><?php echo $result->date; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $result->author; ?> </div><br><br>
                        </div>
                    </div>
                </div> 
        <?php $i++;endforeach; ?>
       </div>   
    </div>
</div>


<script>
    function getData(id){
        var pageId = $(id).val(); 
        $.ajax({
            url: '<?php echo base_url('results') ?>', 
            type:'post',
            data: {id: pageId},
            dataType: 'json',
            success: function(results){ 
        
              var searchresults =""; 
              jQuery.each(results, function( key, val ) {

                searchresults = searchresults + '<div class="col-12">' ;
                searchresults = searchresults + '<div class="card result mb-2" >' ;
                searchresults = searchresults + '<div class="card-body">' ;
                searchresults = searchresults + '<a href="<?php echo base_url(); ?>viewpost/'+val['id'] +'"><b>'+ val['title'] +'</b></a><br>';
                searchresults = searchresults + '<div class="row">' ;
                searchresults = searchresults + '<div class="col-6"><?php echo $result->date; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $result->author; ?> </div><br><br>' ;
                searchresults = searchresults + '</div>' ;
                searchresults = searchresults + '</div>' ;
                searchresults = searchresults + '</div>' ;
                searchresults = searchresults + '</div>' ;
          
              });
              $('#searchresults').html(searchresults);
   
            },     
            error:function(){
                console.log('error');
            }
        });
    }
</script>