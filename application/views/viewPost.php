<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<div class="container pt-5">
  <div class="row">
   <div class="col-6">
    <p  style="font-weight:bold"><?php echo $allData->title;?></p>
   </div> 
  </div>
  <div class="row">
    <div class="col-6"><?php echo $allData->date; ?></div>
    <div class="col-6"><p>Author : <?php echo $allData->author; ?></p></div>
  </div>
  <p><br><?php echo $allData->content ?></p>
</div>

