<?php $this->load->view('components/common/templateHeader'); ?>
<?php $this->load->view('components/sections/navBar'); ?>
<?php $this->load->view('components/sections/bannerSection'); ?>
<?php $this->load->view('components/sections/searchOverlay'); ?>
<?php $this->load->view('components/sections/stickySidebar'); ?>
<?php $this->load->view('components/sections/featuredPosts'); ?>
<?php $this->load->view('components/sections/popularPosts'); ?>
<?php $this->load->view('components/common/templateFooter'); ?>