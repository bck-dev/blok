 <!-- single layout blog content -->
 <?php $this->load->view('components/common/templateHeader'); ?>
<?php $this->load->view('components/sections/navBar'); ?>
<?php $this->load->view('components/sections/searchOverlay'); ?>
<?php $this->load->view('components/sections/stickySidebar'); ?>

 <section class="single-layout">
      <div class="container">
        <div class="blog-img-main">
          <img src="<?php  echo base_url(); ?>uploads/<?php echo $postData->featureImage; ?>" alt="">
        </div>
        <div class="row align-items-center">
          <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
            <div class="blog-content-wrap">
              <div class="blog-title-wrap">
                <div class="author-date">
                  <a class="author" href="<?php echo base_url(); ?>author/<?php echo $postData->author; ?>">
                    <span class=""> <?php echo $postData->author; ?></span>
                  </a>
                  <span><?php echo $postData->date; ?></span>
                </div>
                <ul class="category-tag-list mb-0">
                  <!-- <li class="category-tag-name">
                    <a href="#">Featured</a>
                  </li> -->
                  <li class="category-tag-name">
                    <a href="<?php echo base_url(); ?>category/<?php echo $postData->categoryName; ?>"><?php echo $postData->categoryName;?></a>
                  </li>
                </ul>
                <h1 class="title-font"><?php echo $postData->title;?></h1>
              </div>

              <div class="blog-desc">
                 <?php echo $postData->content ?>
              </div>
              <div class="tags-wrap">
                <div class="blog-tags">
                  <p>Tags:</p>
                  <ul class="sidebar-list tags-list">
                    <?php $i=1; foreach($postData->tags as $tag): ?>
                        <li>
                            <a href="<?php echo base_url(); ?>tag/<?php echo $tag->tagName; ?>">
                                <?php echo $tag->tagName; ?>
                            </a>
                        </li>          
                    <?php  $i++; endforeach; ?>
                  </ul>
                </div>
                
              </div>
              <div class="blog-author-info">
                <div class="author-desc">
                  <small>written by</small>
                  <a href="<?php echo base_url(); ?>author/<?php echo $postData->author; ?>"><h5> <?php echo $postData->author; ?></h5></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Single Layout Blog content end -->

    <!-- Related posts -->
    <!-- <section class="related-posts">
      <div class="container">
        <div class="related-title">
          <h3>Related posts</h3>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-3">
            <div class="card small-card simple-overlay-card">
              <a href="#"><img src="assets/images/town-street.jpg" class="card-img" alt="" /></a>
              <div class="card-img-overlay">
                <ul class="category-tag-list mb-0">
                  <li class="category-tag-name">
                    <a href="#">Travel</a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="#">Why I love to travel in Spring</a>
                </h5>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="card small-card simple-overlay-card">
              <a href="#"><img src="assets/images/camera.jpg" class="card-img" alt="" /></a>
              <div class="card-img-overlay">
                <ul class="category-tag-list mb-0">
                  <li class="category-tag-name">
                    <a href="#">Photography</a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="#">Photography tips and tricks</a>
                </h5>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="card small-card simple-overlay-card">
              <a href="#"><img src="assets/images/winter-house.jpg" class="card-img" alt="" /></a>
              <div class="card-img-overlay">
                <ul class="category-tag-list mb-0">
                  <li class="category-tag-name">
                    <a href="#">Lifestyle</a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="#">Living in a beach house</a>
                </h5>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="card small-card simple-overlay-card">
              <a href="#"><img src="assets/images/shoes.jpg" class="card-img" alt="" /></a>
              <div class="card-img-overlay">
                <ul class="category-tag-list mb-0">
                  <li class="category-tag-name">
                    <a href="#">Travel</a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="#">The next travel destination</a>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- Related posts end -->
    <?php $this->load->view('components/common/templateFooter'); ?>