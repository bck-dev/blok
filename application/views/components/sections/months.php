<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<div class="container pt-5">
    <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Months</span>     
            </h4>
            <ul class="list-group mb-3">
            <?php for($i=0;$i<count($monthData);$i++): ?>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0"><a href="<?php echo base_url(); ?>monthposts/<?php echo $monthData[$i]['month']; ?>"><?php echo $monthData[$i]['month'] ?></a></h6>
                    </div>
                    <span style="background-color: #6c757d;padding-right: 0.6em;padding-left: 0.6em;border-radius: 10rem;">
                        <?php  echo $monthData[$i]['count'];; ?>
                    </span>
                </li>          
            <?php  endfor; ?>
            </ul>    
        </div>
    </div>
</div>
