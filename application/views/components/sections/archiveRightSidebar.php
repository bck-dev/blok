 <!-- Archive content -->
 <section class="archive-content">
            <div class="container">
                <div class="row " id="main-content">
                    <div class="col-md-7 col-lg-8 content">
                        <!-- Archive posts -->
                        <div class="archive-posts theiaStickySidebar">
                            <div class="card border-0 mb-5">
                                <div class="row no-gutters align-items-center align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/town-street.jpg" class="card-img"
                                                alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Travel</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">Top 10 tourist destinations of
                                                    the world</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>

                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/coach.jpg" class="card-img" alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Lifestyle</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">Old yet beautiful things</a>
                                            </h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/shoes.jpg" class="card-img" alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Lifestyle</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">I want to leave my footprints
                                                    all over the world</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/holidays.jpg" class="card-img" alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Lifestyle</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">How to make most out of
                                                    vaccation</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/lighthouse.jpg" class="card-img"
                                                alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Travel</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">7 things you don't know about
                                                    Light house</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/blonde-girl.jpg" class="card-img"
                                                alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Travel</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">Why alone time is a must for
                                                    you</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-5">
                                <div class="row no-gutters align-items-center align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/town-street.jpg" class="card-img"
                                                alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Travel</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">Top 10 tourist destinations of
                                                    the world</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>

                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Archive posts end -->
                    </div>
                    <div class="col-md-5 col-lg-4 sidebar">
                        <!-- Sidebar posts -->
                        <div id="sticker" class="theiaStickySidebar">
                            <div class="sidebar-posts">
                                <div class="sidebar-title">
                                    <h5><i class="fas fa-circle"></i>Popular Posts</h5>
                                </div>
                                <div class="sidebar-content p-0">
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/sea-lighthouse.jpg" class="card-img" alt="">
                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">
                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">Lighthouse since
                                                            ages</a>
                                                    </h5>
                                                    <div class="author-date">
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/paris.jpg" class="card-img" alt="">
                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">
                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">5 things you should
                                                            not miss about Paris</a>
                                                    </h5>
                                                    <div class="author-date">
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/orange-bus.jpg" class="card-img" alt="">
                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">
                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">5 reasons for
                                                            travelling more</a>
                                                    </h5>
                                                    <div class="author-date">
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/city-pink.jpg" class="card-img" alt="">

                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">

                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">Pink city</a>
                                                    </h5>
                                                    <div class="author-date">

                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/umbrella.jpg" class="card-img" alt="">
                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">

                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">Top 10 tips with
                                                            common lifestyles</a>
                                                    </h5>
                                                    <div class="author-date">

                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="popular-tags mt-4">
                                <div class="sidebar-title">
                                    <h5><i class="fas fa-circle"></i>Popular tags</h5>
                                </div>
                                <div class="sidebar-content">
                                    <ul class="sidebar-list tags-list">
                                        <li><a href="#">Food</a></li>
                                        <li><a href="#">Technology</a></li>
                                        <li><a href="#">UI/UX</a></li>
                                        <li><a href="#">Diet
                                            </a></li>
                                        <li><a href="#">Lovelife</a></li>
                                        <li><a href="#">Jobs</a></li>
                                        <li><a href="#">Popular</a></li>
                                        <li><a href="#">health</a></li>
                                        <li><a href="#">VisitNepal</a></li>
                                        <li><a href="#">travel</a></li>
                                        <li><a href="#">freelance</a></li>
                                        <li><a href="#">leadership</a></li>
                                        <li><a href="#">happiness</a></li>
                                    </ul>
                                </div>

                            </div>
                            <div class="sidebar-posts mt-4">
                                <div class="sidebar-title">
                                    <h5><i class="fas fa-circle"></i>Archive</h5>
                                </div>
                                <div class="sidebar-content">
                                    <ul class="archive-date-list">
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> December
                                                2019</a></li>
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> November
                                                2019</a></li>
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> October
                                                2019</a></li>
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> September
                                                2019</a></li>
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> August
                                                2019</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="sidebar-posts mt-4">
                                <div class="sidebar-title">
                                    <h5><i class="fas fa-circle"></i>Categories</h5>
                                </div>
                                <div class="sidebar-content">
                                    <div class="category-name-list">
                                        <div class="card small-card">
                                            <a href="#"><img src="assets/images/shoes.jpg" class="card-img"
                                                    alt="" /></a>
                                            <div class="card-img-overlay">

                                                <h5 class="card-title title-font mb-0">
                                                    <a href="#">Travel</a>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="card small-card">
                                            <a href="#"><img src="assets/images/photography.jpg" class="card-img"
                                                    alt="" /></a>
                                            <div class="card-img-overlay">

                                                <h5 class="card-title title-font mb-0">
                                                    <a href="#">Photography</a>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="card small-card">
                                            <a href="#"><img src="assets/images/fashion.jpg" class="card-img"
                                                    alt="" /></a>
                                            <div class="card-img-overlay">

                                                <h5 class="card-title title-font mb-0">
                                                    <a href="#">Fashion</a>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="card small-card">
                                            <a href="#"><img src="assets/images/tech.jpg" class="card-img" alt="" /></a>
                                            <div class="card-img-overlay">
                                                <h5 class="card-title title-font mb-0">
                                                    <a href="#">Technology</a>
                                                </h5>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Sidebar posts end -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Archive content end -->