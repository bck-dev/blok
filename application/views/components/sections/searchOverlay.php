 <!-- Search overlay -->
 <div id="search-overlay" class="search-section">
        <span class="closebtn"><i class="fas fa-times"></i></span>
        <div class="overlay-content">
          <form  action="<?php echo base_url('search');?>" method="GET">
            <input type="text" placeholder="Search here" name="keyword">
            <button type="submit"><i class="fa fa-search"></i></button>
          </form>  

        </div>
</div>
      <!-- Search overlay end -->