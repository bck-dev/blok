<body>
  <!-- Preloader -->
  <div class="preloader-wrapper">
    <div class="preloader">
      <div class="preloader-circle" id="status">
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  </div>
  <!-- Preloader end -->

  <!-- Header -->
  <header>
    <!-- Top header -->
    <div class="top-header">
      <div class="container">
        <div class="row  align-items-center">
          <div class="col-md-3">
            <div class="social-links">
              <ul>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-5">
            <div class="brand-name text-center">
              <a href="index.html">
                <h1>Kavya</h1>
                <span>Enter your tagline here</span>
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="search-wrapper">
              <div class="search-icon">
                <button class="open-search-btn"><i class="fa fa-search"></i></button>
              </div>
              <div class="sidebar-icon">
                <button class="sidebar-btn"> <i class="fas fa-ellipsis-v"></i></button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    
    <!-- Top header end -->