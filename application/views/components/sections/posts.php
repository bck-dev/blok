
<?php $i=1; foreach($postData as $post): ?>
  <div class="card border-0 mb-5">
    <div class="row no-gutters align-items-center align-items-center">
      <div class="col-md-5">
        <a href="<?php echo base_url(); ?>viewpost/<?php echo $post->id; ?>">
          <img src="<?php  echo base_url(); ?>uploads/<?php echo $post->featureImage; ?>" class="card-img" alt="<?php echo $post->title; ?>">
        </a>
      </div>
      <div class="col-md-7">
        <div class="card-body">
          <ul class="category-tag-list">
            <li class="category-tag-name">
                <a href="<?php echo base_url(); ?>category/<?php echo $post->categoryName; ?>"><?php echo $post->categoryName; ?></a>
            </li>
          </ul>
          <h5 class="card-title title-font">
            <a href="<?php echo base_url(); ?>viewpost/<?php echo $post->id; ?>"><?php echo $post->title; ?></a>
          </h5>
          <p class="card-text"><?php echo $post->shortDescription; ?><p>

          <div class="author-date">
            <a class="author" href="<?php echo base_url(); ?>author/<?php echo $post->author; ?>">
              <span class="writer-name-small"><?php echo $post->author; ?></span>
            </a>
              <span><?php echo $post->date; ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $i++; endforeach; ?>


<?php //$this->load->view('components/common/footer'); ?> 

<script>
    function getData(id){
        var pageId = $(id).val(); 
        $.ajax({
            url: '<?php echo base_url('posts') ?>', 
            type:'post',
            data: {id: pageId},
            dataType: 'json',
            success: function(results){ 
              var posts =""; 
              jQuery.each(results, function( key, val ) {
                posts = posts + '<div class="row py-5">';
                posts = posts + '<div class="col-3" >';
                posts = posts + '<img src="<?php echo base_url(); ?>/uploads/'+ val['featureImage'] +'" class="w-100" />';
                posts = posts + '</div>';
                posts = posts + '<div class="col-9">';
                posts = posts + '<a href="<?php echo base_url(); ?>viewpost/'+ val['id'] +'" ><b>'+ val['title'] +'</b></a><br>';
                posts = posts + '<div class="row">';
                posts = posts + '<div class="col-6">'+ val['date'] +'</div>';
                posts = posts + '<div class="col-6">'+ val['author'] +'</p></div><br><br>';
                posts = posts + '</div>';
                posts = posts + val['shortDescription'];
                posts = posts + '<a class="btn btn-info btn-sm"  href="<?php echo base_url(); ?>viewpost/'+ val['id'] +'" style="background-color:#17a2b8;color:white;">Read More</a>';
                posts = posts + '</div>';
                posts = posts + '</div>';
              });
              $('#posts').html(posts);
            },     
            error:function(){
                console.log('error');
            }
        });
    }
</script>
