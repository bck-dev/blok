<!-- Popular posts -->
<section class="popular-posts section-padding">
      <div class="container">
        <div class="section-title">
          <h2>Popular Posts</h2>
        </div>
        <div class="row">
          <div class="col-md-7 col-lg-8">
            <div class="row">
              
            <?php foreach($popularPosts as $popularPost): ?>
            <div class="col-md-6">
                <div class="card">
                  <a href="<?php echo base_url(); ?>viewpost/<?php echo $popularPost->id; ?>">
                    <img src="<?php  echo base_url(); ?>uploads/<?php echo $popularPost->featureImage; ?>" class="card-img-top" alt="" />
                  </a>

                  <div class="card-body px-0">
                    <ul class="category-tag-list">
                      <li class="category-tag-name">
                        <a href="<?php echo base_url(); ?>category/<?php echo $popularPost->categoryName->{'categoryName'}; ?>"><?php echo $popularPost->categoryName->{'categoryName'}; ?></a>
                      </li>
                    </ul>

                    <h5 class="card-title title-font">
                      <a href="<?php echo base_url(); ?>viewpost/<?php echo $popularPost->id; ?>">
                      <?php echo $popularPost->title; ?></a>
                    </h5>
                    <div class="author-date">
                      <a class="author" href="<?php echo base_url(); ?>author/<?php echo $popularPost->author; ?>">
                        <span class="writer-name-small"><?php echo $popularPost->author; ?></span>
                      </a>
                      <span><?php echo $popularPost->date; ?></a></span>
                    </div>
                  </div>
                </div>
              </div>
              <?php endforeach; ?>
            </div>
          </div>

          <div class="col-md-5 col-lg-4">

            <div class="sidebar-posts">

              <div class="sidebar-title">
                <h5><i class="fas fa-circle"></i> Recent Posts</h5>
              </div>

              <div class="sidebar-content author-posts">

              <?php foreach($recentPosts as $recentPost): ?>
                <div class="card mb-3">
                  <div class="row no-gutters align-items-center">
                    <div class="col-4 col-md-4">
                      <a href="<?php echo base_url(); ?>viewpost/<?php echo $recentPost->id; ?>">
                        <img src="<?php  echo base_url(); ?>uploads/<?php echo $recentPost->featureImage; ?>" class="card-img" alt="">
                      </a>
                    </div>
                    <div class="col-8 col-md-8">
                      <div class="card-body">
                        <ul class="category-tag-list mb-0">
                          <li class="category-tag-name">
                            <a href="<?php echo base_url(); ?>category/<?php echo $recentPost->categoryName->{'categoryName'}; ?>"><?php echo $recentPost->categoryName->{'categoryName'}; ?></a>
                          </li>
                        </ul>
                        <h5 class="card-title title-font"><a href="<?php echo base_url(); ?>viewpost/<?php echo $recentPost->id; ?>"><?php echo $recentPost->title; ?></a>
                        </h5>
                        <div class="author-date">
                          <span><?php echo $recentPost->date; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php endforeach; ?>
              </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Popular posts end -->