 <!-- Footer section -->
 <footer class="footer-section">
    <div class="container">
      <div class="footer-content">
        <div class="footer-logo">
          <a href="#">
            <h5 class="brand-name">Title</h5>
          </a>
        </div>
        <div class="footer-copyright">
          <p>&copy;2020 Title. All rights reserved. Developed by <a href="#">Bckonnect</a> </p>
        </div>
        <div class="social-links">
          <ul>
            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer section end -->

  <!-- Scroll to top -->
  <div id="stop" class="scroll-to-top">
    <span><a href="#"><i class="fas fa-arrow-up"></i></a></span>
  </div>
  <!-- Scroll to top end -->

  <!-- Javascript -->
  <script src="assets/js/jquery-3.4.1.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/slick.min.js"></script>
  <script src="assets/js/jquery.sticky.js"></script>
  <script src="assets/js/ResizeSensor.min.js"></script>
  <script src="assets/js/theia-sticky-sidebar.min.js"></script>
  <script src="assets/js/main.js"></script>
</body>

</html>