    <!-- Banner section -->
    <section class="banner-section">
      <div class="container">
        <div class="banner-carousel">
        <?php foreach($sliderPosts as $sliderPost): ?>
          <div class="banner-item">
            <div class="card">
           
              <a href="<?php echo base_url(); ?>viewpost/<?php echo $sliderPost->id; ?>"><img src="<?php  echo base_url(); ?>uploads/<?php echo $sliderPost->featureImage; ?>" class="card-img" alt="" /></a>
              <div class="card-img-overlay banner-text slider-customization">
                <ul class="category-tag-list">
                  <li class="category-tag-name">
                    <a href="<?php echo base_url(); ?>category/<?php echo $sliderPost->categoryName->{'categoryName'}; ?>"><?php echo $sliderPost->categoryName->{'categoryName'}; ?></a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="<?php echo base_url(); ?>viewpost/<?php echo $sliderPost->id; ?>"><?php echo $sliderPost->title; ?></a>
                </h5>
                <p class="card-text mb-3">
                  <?php echo $sliderPost->shortDescription; ?>
                </p><br>
                <a href="<?php echo base_url(); ?>viewpost/<?php echo $sliderPost->id; ?>" class="btn btn-solid btn-read">Read More</a>
              </div>
            
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </section>
    <!-- Banner section end -->