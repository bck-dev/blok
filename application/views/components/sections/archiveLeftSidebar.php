        <!-- page header end -->
        <section class="archive-content">
            <div class="container">
                <div class="row main" id="main-content">
                    <div class="col-md-5 col-lg-4 order-2 order-md-1 sidebar">
                        <div id="sticker" class="theiaStickySidebar">
                            <div class="sidebar-posts">
                                <div class="sidebar-title">
                                    <h5><i class="fas fa-circle"></i>Popular Posts</h5>
                                </div>
                                <div class="sidebar-content p-0">
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/sea-lighthouse.jpg" class="card-img" alt="">
                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">

                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">Lighthouse since
                                                            ages</a>
                                                    </h5>
                                                    <div class="author-date">
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/paris.jpg" class="card-img" alt="">

                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">

                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">5 things you should
                                                            not miss about Paris</a>
                                                    </h5>
                                                    <div class="author-date">

                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/orange-bus.jpg" class="card-img" alt="">
                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">
                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">5 reasons for
                                                            travelling more</a>
                                                    </h5>
                                                    <div class="author-date">
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/city-pink.jpg" class="card-img" alt="">
                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">

                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">Pink city</a>
                                                    </h5>
                                                    <div class="author-date">
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card border-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-3 col-md-3">
                                                <a href="#">
                                                    <img src="assets/images/umbrella.jpg" class="card-img" alt="">
                                                </a>
                                            </div>
                                            <div class="col-9 col-md-9">
                                                <div class="card-body">
                                                    <ul class="category-tag-list mb-0">
                                                        <li class="category-tag-name">
                                                            <a href="#">Lifestyle</a>
                                                        </li>
                                                    </ul>
                                                    <h5 class="card-title title-font"><a href="#">Top 10 tips with
                                                            common lifestyles</a>
                                                    </h5>
                                                    <div class="author-date">
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sidebar-posts mt-4">
                                <div class="sidebar-title">
                                    <h5><i class="fas fa-circle"></i>Archive</h5>
                                </div>
                                <div class="sidebar-content">
                                    <ul class="archive-date-list">
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> December
                                                2019</a></li>
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> November
                                                2019</a></li>
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> October
                                                2019</a></li>
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> September
                                                2019</a></li>
                                        <li><a href="#"><img src="assets/images/archive-icon.svg" alt=""> August
                                                2019</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="recent-posts mt-4">
                                <div class="sidebar-title">
                                    <h5><i class="fas fa-circle"></i>Trending this week</h5>
                                </div>
                                <div class="sidebar-content">
                                    <ul class="sidebar-list">
                                        <li class="sidebar-item">
                                            <div class="num-left">
                                                1
                                            </div>
                                            <div class="content-right">
                                                <a href="#">Healthy Dieting Habits for any age</a>

                                            </div>
                                        </li>
                                        <li class="sidebar-item">
                                            <div class="num-left">
                                                2
                                            </div>
                                            <div class="content-right">
                                                <a href="#"> 10 foods you should try in Kathmandu</a>
                                            </div>
                                        </li>
                                        <li class="sidebar-item">
                                            <div class="num-left">
                                                3
                                            </div>
                                            <div class="content-right">
                                                <a href="#">Everything You Wanted to Know About Love Life</a>
                                            </div>
                                        </li>
                                        <li class="sidebar-item">
                                            <div class="num-left">
                                                4
                                            </div>
                                            <div class="content-right">
                                                <a href="#">Yoga works only under these conditions</a>
                                            </div>
                                        </li>
                                        <li class="sidebar-item">
                                            <div class="num-left">
                                                5
                                            </div>
                                            <div class="content-right">
                                                <a href="#">Top 10 mysterious places you didn't know</a>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-8 order-1 order-md-2 content">
                        <div class="archive-posts theiaStickySidebar ">
                            <div class="card border-0 mb-3 mb-lg-5">
                                <div class="row no-gutters align-items-center align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/town-street.jpg" class="card-img"
                                                alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Travel</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">Top 10 tourist destinations of
                                                    the world</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>

                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-3 mb-lg-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/coach.jpg" class="card-img" alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Lifestyle</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">Old yet beautiful things</a>
                                            </h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-3 mb-lg-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/shoes.jpg" class="card-img" alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Lifestyle</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">I want to leave my footprints
                                                    all over the world</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-3 mb-lg-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/holidays.jpg" class="card-img" alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Lifestyle</a>
                                                </li>

                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">How to make most out of
                                                    vaccation</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-0 mb-3 mb-lg-5">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-5">
                                        <a href="#"> <img src="assets/images/lighthouse.jpg" class="card-img"
                                                alt=""></a>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <ul class="category-tag-list">
                                                <li class="category-tag-name">
                                                    <a href="#">Travel</a>
                                                </li>
                                            </ul>
                                            <h5 class="card-title title-font"><a href="#">7 things you don't know about
                                                    Light house</a></h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Quis ipsum rem, delectus
                                                deserunt consectetur saepe? Expedita sapiente rerum nostrum fuga non
                                                iure minima sunt inventore.<p>
                                                    <div class="author-date">
                                                        <a class="author" href="#">
                                                            <img src="assets/images/writer.jpg" alt=""
                                                                class="rounded-circle" />
                                                            <span class="writer-name-small">Julie</span>
                                                        </a>
                                                        <a class="date" href="#">
                                                            <span>21 Dec, 2019</span>
                                                        </a>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </section>
