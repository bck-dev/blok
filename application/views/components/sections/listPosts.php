<?php $this->load->view('components/common/templateHeader'); ?>
<?php $this->load->view('components/sections/navBar'); ?>

 <!-- Archive content -->
 <section class="archive-content mt-5">
    <div class="container">
        <div class="row " id="main-content">
            <div class="col-md-7 col-lg-8 content">
                <!-- Archive posts -->
                <div class="archive-posts theiaStickySidebar">
                    <?php $this->load->view('components/sections/posts'); ?>
                </div>
                <!-- Archive posts end -->
            </div>
            <div class="col-md-5 col-lg-4 sidebar">
                <!-- Sidebar posts -->
                <div id="sticker" class="theiaStickySidebar">
                    
                    <div class="sidebar-posts mt-4">
                        <div class="sidebar-title">
                            <h5><i class="fas fa-circle"></i>Categories</h5>
                        </div>
                        <div class="sidebar-content">
                            <ul class="archive-date-list">
                                <?php $i=1; foreach($categories as $category): if($category->count>0):?>
                                    <li class="drop-menu-item mb-3">
                                        <a href="<?php echo base_url(); ?>category/<?php echo $category->category; ?>">
                                            <h5 class="card-title title-font">
                                                <?php echo $category->category; ?>
                                                <span style="margin-left: 1rem; font-size: 1rem; background-color: #ededed; padding: 0.25rem 1rem; border-radius: 1rem;">
                                                    <?php echo $category->count; ?>
                                                </span>
                                            </h5>
                                        </a>
                                    </li>          
                                <?php endif; $i++; endforeach; ?>
                            </ul>
                        </div>
                    </div>

                    <div class="sidebar-posts mt-4">
                        <div class="sidebar-title">
                            <h5><i class="fas fa-circle"></i>Tags</h5>
                        </div>
                        <div class="sidebar-content">
                            <div class="tags-wrap">
                                <ul class="sidebar-list tags-list">
                                    <?php $i=1; foreach($tags as $tag): if($tag->count>0):?>
                                        <li>
                                            <a href="<?php echo base_url(); ?>tag/<?php echo $tag->tagName; ?>">
                                                <?php echo $tag->tagName; ?>
                                                <span class="ml-3">
                                                    <?php echo $tag->count; ?>
                                                </span>
                                            </a>
                                        </li>          
                                    <?php endif; $i++; endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- Sidebar posts end -->
            </div>
        </div>
    </div>
</section>
<!-- Archive content end -->

<?php $this->load->view('components/common/templateFooter'); ?>