<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<?php $i=1; foreach($postData as $post): ?>
<div class="container pt-5 pb-5">
  <div class="row">
    <div class="col-3" >
     <img src="<?php echo base_url(); ?>/uploads/<?php echo $post->featureImage; ?>" width="200px" />
    </div>
    <div class="col-6">
     <a href="<?php echo base_url(); ?>viewpost/<?php echo $post->id; ?>"><b><?php echo $post->title; ?></b></a><br>
     <div class="row">
      <div class="col-6"><?php echo $post->date; ?></div>
      <div class="col-6"><p>Author : <?php echo $post->author; ?></p></div><br><br>
     </div>
     <?php echo $post->shortDescription; ?>
        <a class="btn btn-info btn-sm"  href="<?php echo base_url(); ?>viewpost/<?php echo $post->id; ?>" style="background-color:#17a2b8;color:white;">Read More</a>
    </div>
  </div>
</div>
<?php  $i++; endforeach; ?>