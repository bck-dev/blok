<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<div class="container pt-5">
    <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Authors</span>     
            </h4>
            <ul class="list-group mb-3">
            <?php $i=1; foreach($authors as $author): ?>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0"><a href="<?php echo base_url(); ?>authorposts/<?php echo $author->id; ?>"><?php echo $author->fullName; ?></a></h6>
                    </div>
                    <span style="background-color: #6c757d;padding-right: 0.6em;padding-left: 0.6em;border-radius: 10rem;">
                        <?php echo $author->count; ?>
                    </span>
                </li>          
            <?php  $i++; endforeach; ?>
            </ul>    
        </div>
    </div>
</div>