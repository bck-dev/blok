<body>
  <!-- Preloader  -->
  <div class="preloader-wrapper">
    <div class="preloader">
      <div class="preloader-circle" id="status">
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  </div>
  <!-- Preloader end -->

  <main class="kavya-home-2">
    <!-- Header -->
    <header>
      <!-- Navbar -->
      <nav>
        <div class=" kavya-navbar-2 kavya-navbar" id="sticky-top">
          <div class="container">
            <div class="row align-items-center">
              <div class="col order-2 col-lg-2 ">
                <div class="brand-name">
                  <a href="<?php echo base_url('home'); ?>">
                    <img src="<?php echo base_url('assets/images/logo.png'); ?>" class="nav-logo" />
                  </a>
                </div>
              </div>
              <div class="col col-lg-8 order-1 order-lg-2">
                <span class="navbar-toggle" id="navbar-toggle">
                  <i class="fas fa-bars"></i>
                </span>
                <ul class="nav-menu ml-auto mr-auto" id="nav-menu-toggle">
                  <li class="nav-item"><a href="<?php echo base_url('home'); ?>" class="nav-link">Home</a></li>
                  <!-- <li class="nav-item"><a href="#" class="nav-link">Categories <span class="arrow-icon"> <span
                          class="left-bar"></span>
                        <span class="right-bar"></span></span>
                    </a>
                    <ul class="drop-menu">
                      <?php $i=1; foreach($categories as $category): ?>
                        <li class="drop-menu-item">
                          <a href="<?php echo base_url(); ?>category/<?php echo $category->category; ?>">
                            <?php echo $category->category; ?>
                          </a>
                        </li>          
                      <?php  $i++; endforeach; ?>
                    </ul>
                  </li> -->
                  <?php $i=1; foreach($categories as $category): ?>
                    <li class="nav-item">
                      <a href="<?php echo base_url(); ?>category/<?php echo $category->category; ?>" class="nav-link">
                        <?php echo $category->category; ?>
                      </a>
                    </li>          
                  <?php  $i++; endforeach; ?>
                </ul>
              </div>
              <div class="col col-lg-2 order-3">
                
                <div class="search-wrapper">
                  <a class="d-none d-lg-block" href="<?php if($_SESSION['blogFor']=="UC"): echo "https://www.pediatricafterhour.com/appointment"; else: echo "https://www.ktdoctor.com/appointment"; endif; ?>" target="_blank">
                    <div class="btn btn-primary appointment">
                      APPOINTMENT
                    </div>
                  </a>
                  <div class="search-icon">
                    <button class="open-search-btn"><i class="fa fa-search"></i></button>
                  </div>
                  <!-- <div class="sidebar-icon">
                    <button class="sidebar-btn"> <i class="fas fa-ellipsis-v"></i></button>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
          <a class="mt-3 d-block d-lg-none" href="<?php if($_SESSION['blogFor']=="UC"): echo "https://www.pediatricafterhour.com/appointment"; else: echo "https://www.ktdoctor.com/appointment"; endif; ?>" target="_blank">
            <div class="btn btn-primary w-100 appointment">
              APPOINTMENT
            </div>
          </a>
        </div>
      </nav>
      
