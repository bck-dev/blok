 <!-- Instagram Posts -->
 <section class="insta-posts">
    <div class="container">
      <div class="insta-title">
        <img src="assets/images/instagram.svg" alt="">
        <h2>Follow us on Instagram</h2>
      </div>

      <div class="image-item-grid insta-slider">

        <div class="image-item">
          <a href="#">
            <img src="assets/images/beautiful-girl.jpg" alt="">
          </a>
          <a href="#">
            <div class="image-hover">
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </div>
        <div class="image-item image-item-margin">
          <a href="#">
            <img src="assets/images/colors.jpg" alt="">
          </a>
          <a href="#">
            <div class="image-hover">
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </div>
        <div class="image-item">
          <a href="#">
            <img src="assets/images/city-pink.jpg" alt="">
          </a>
          <a href="#">
            <div class="image-hover">
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </div>
        <div class="image-item image-item-margin">
          <a href="#">
            <img src="assets/images/sea-lighthouse.jpg" alt="">
          </a>
          <a href="#">
            <div class="image-hover">
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </div>
        <div class="image-item">
          <a href="#">
            <img src="assets/images/boat.jpg" alt="">
          </a>
          <a href="#">
            <div class="image-hover">
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </div>
        <div class="image-item">
          <a href="#">
            <img src="assets/images/beach-sea.jpg" alt="">
          </a>
          <a href="#">
            <div class="image-hover">
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </div>
        <div class="image-item">
          <a href="#">
            <img src="assets/images/orange-bus.jpg" alt="">
          </a>
          <a href="#">
            <div class="image-hover">
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </div>
        <div class="image-item">
          <a href="#">
            <img src="assets/images/tall.jpg" alt="">
          </a>
          <a href="#">
            <div class="image-hover">
              <i class="fas fa-heart"></i>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  <!-- Instagram posts end -->