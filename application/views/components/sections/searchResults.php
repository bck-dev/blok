 <!-- search results -->
<?php $this->load->view('components/common/templateHeader'); ?>
<?php $this->load->view('components/sections/navBar'); ?>
<?php $this->load->view('components/sections/searchOverlay'); ?>
<?php $this->load->view('components/sections/stickySidebar'); ?>
<?php $this->load->view('components/sections/breadCrumb'); ?>

<section class="search-results">
      <div class="container">
        <h5 class="search-result-title">We've found <?php if($count>0): echo $count; else: echo "0"; endif; ?> results for "<?php echo $keyword ?>"</h5>
        <?php $this->load->view('components/common/pagination'); ?>
        <div id="searchresults" class="row align-items-center">

        <?php $i=1; foreach($postData as $result): ?>
          <div class="col-md-12 col-lg-6">
            <div class="card p-3">
              <div class="row no-gutters align-items-center">
                <div class="col-md-5">
                  <a href="<?php echo base_url(); ?>viewpost/<?php echo $result->id; ?>"> <img src="<?php  echo base_url(); ?>uploads/<?php echo $result->featureImage; ?>" class="card-img" alt=""></a>
                </div>
                <div class="col-md-7">
                  <div class="card-body">
                    <ul class="category-tag-list">
                      <li class="category-tag-name">
                        <a href="#">Results <?php echo $i; ?></a>
                      </li>
                    </ul>
                    <h5 class="card-title title-font"><a href="<?php echo base_url(); ?>viewpost/<?php echo $result->id; ?>"><b><?php echo $result->title; ?></b></a></h5>

                  </div>
                </div>
              </div>
            </div>
            </div>
        <?php $i++; endforeach; ?>
        
        </div>
      </div>
</section>

    <?php $this->load->view('components/common/templateFooter'); ?>

    <script>
    function getData(id){
        var pageId = $(id).val(); 
        $.ajax({
            url: '<?php echo base_url('results') ?>', 
            type:'post',
            data: {id: pageId},
            dataType: 'json',
            success: function(results){ 
        
              var searchresults =""; 
              jQuery.each(results, function( key, val ) {

                searchresults = searchresults + '<div  class="col-md-12 col-lg-6">' ;
                searchresults = searchresults + '<div class="card p-3">' ;
                searchresults = searchresults + '<div class="row no-gutters align-items-center">' ;
                searchresults = searchresults + '<div class="col-md-5">';
                searchresults = searchresults + '<a href="<?php echo base_url(); ?>viewpost/'+val['id']+'">' ;
                searchresults = searchresults + '<img src="<?php  echo base_url(); ?>uploads/'+val['featureImage']+'" class="card-img" alt=""></a>';
                searchresults = searchresults + '</div>' ;
                searchresults = searchresults + '<div class="col-md-7">' ;
                searchresults = searchresults + '<div class="card-body">' ;
                searchresults = searchresults + '<ul class="category-tag-list">' ;
                searchresults = searchresults + '<li class="category-tag-name">' ;
                searchresults = searchresults + '<a href="#">Travel</a>' ;
                searchresults = searchresults + '</li>' ;
                searchresults = searchresults + '</ul>' ;
                searchresults = searchresults + '<h5 class="card-title title-font"><a href="<?php echo base_url(); ?>viewpost/'+val['id']+'"><b>'+val['title']+'</b></a></h5>' ;
                searchresults = searchresults + '<p class="card-text"><b>'+val['shortDescription']+'</b></p>' ;
                searchresults = searchresults + '</div>' ;
                searchresults = searchresults + '</div>' ;
                searchresults = searchresults + '</div>' ;
                searchresults = searchresults + '</div>' ;
                searchresults = searchresults + '</div>' ;
          
              });
              $('#searchresults').html(searchresults);
   
            },     
            error:function(){
                console.log('error');
            }
        });
    }
</script>