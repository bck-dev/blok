  <!-- Featured posts -->
  <section class="featured-posts mt-5">
      <div class="container">
        <div class="section-title">
          <h2>Featured posts</h2>
        </div>
        <div class="row">
            
          <?php $i=0;foreach($featuredPosts as $featuredPost): $i++;?>
          <?php if($i%3==0): ?>
            <?php  $i++;?>
          <?php  endif;?>
          <?php if($i%2 !=0): ?>
          <div class="col-md-7">
          <?php else:?>
          <div class="col-md-5">
          <?php  endif;?>
            <div class="card simple-overlay-card">
              <a href="<?php echo base_url(); ?>viewpost/<?php echo $featuredPost->id; ?>"><img src="<?php  echo base_url(); ?>uploads/<?php echo $featuredPost->featureImage; ?>" class="card-img" alt="" /></a>
              <div class="card-img-overlay">
                <ul class="category-tag-list">
                  <li class="category-tag-name">
                    <a href="<?php echo base_url(); ?>category/<?php echo $featuredPost->categoryName->{'categoryName'}; ?>"><?php echo $featuredPost->categoryName->{'categoryName'}; ?></a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="<?php echo base_url(); ?>viewpost/<?php echo $featuredPost->id; ?>"><?php echo $featuredPost->title; ?></a>
                </h5>
              </div>
            </div>
          </div>
    
          <?php endforeach; ?>
        </div>
      </div>
  </section>
    <!-- Features posts end -->