 <!-- Archive content -->
 <section class="archive-content">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="card border-0">
              <a href="#">
                <img src="assets/images/town-street.jpg" class="card-img-top" alt="" />
              </a>
              <div class="card-body px-0">
                <ul class="category-tag-list">
                  <li class="category-tag-name">
                    <a href="#">Featured</a>
                  </li>
                  <li class="category-tag-name">
                    <a href="#">Travel</a>
                  </li>
                </ul>

                <h5 class="card-title title-font">
                  <a href="#">Why I love to travel in Spring</a>
                </h5>
                <p class="card-text pb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit
                  dolore nam ut distinctio reiciendis cupiditate, repellendus unde quis rerum, aliquid
                  esse quaerat! Vel laboriosam illo minima commodi nam, omnis dolores iusto facere.
                  Maxime unde, modi dolorum minima veritatis quos soluta.</p>
                <div class="author-date">
                  <a class="author" href="#">
                    <img src="assets/images/person1.jpg" alt="" class="rounded-circle" />
                    <span class="writer-name-small">Sallie Allen
                    </span>
                  </a>
                  <a class="date" href="#">
                    <span>21 Dec, 2019</span>
                  </a>
                </div>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card border-0">
              <a href="#">
                <img src="assets/images/woods.jpg" class="card-img-top" alt="" />
              </a>

              <div class="card-body px-0">
                <ul class="category-tag-list">
                  <li class="category-tag-name">
                    <a href="#">Nature</a>
                  </li>
                </ul>

                <h5 class="card-title title-font">
                  <a href="#">You will get lost in these woods</a>
                </h5>
                <p class="card-text pb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit
                  dolore nam ut distinctio reiciendis cupiditate, repellendus unde quis rerum, aliquid
                  esse quaerat! Vel laboriosam illo minima commodi nam, omnis dolores iusto facere.
                  Maxime unde, modi dolorum minima veritatis quos soluta.</p>
                <div class="author-date">
                  <a class="author" href="#">
                    <img src="assets/images/person1.jpg" alt="" class="rounded-circle" />
                    <span class="writer-name-small">Sallie Allen
                    </span>
                  </a>
                  <a class="date" href="#">
                    <span>21 Dec, 2019</span>
                  </a>
                </div>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card border-0">
              <a href="#">
                <img src="assets/images/blonde-girl.jpg" class="card-img-top" alt="" />
              </a>

              <div class="card-body px-0">
                <ul class="category-tag-list">
                  <li class="category-tag-name">
                    <a href="#">Lifestyle</a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="#">Why alone time is a must for you</a>
                </h5>
                <p class="card-text pb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit
                  dolore nam ut distinctio reiciendis cupiditate, repellendus unde quis rerum, aliquid
                  esse quaerat! Vel laboriosam illo minima commodi nam, omnis dolores iusto facere.
                  Maxime unde, modi dolorum minima veritatis quos soluta.</p>
                <div class="author-date">
                  <a class="author" href="#">
                    <img src="assets/images/person1.jpg" alt="" class="rounded-circle" />
                    <span class="writer-name-small">Sallie Allen
                    </span>
                  </a>
                  <a class="date" href="#">
                    <span>21 Dec, 2019</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card border-0">
              <a href="#">
                <img src="assets/images/london.jpg" class="card-img-top" alt="" />
              </a>
              <div class="card-body px-0">
                <ul class="category-tag-list">
                  <li class="category-tag-name">
                    <a href="#">Nature</a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="#">10 things you should not miss about London</a>
                </h5>
                <p class="card-text pb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit
                  dolore nam ut distinctio reiciendis cupiditate, repellendus unde quis rerum, aliquid
                  esse quaerat! Vel laboriosam illo minima commodi nam, omnis dolores iusto facere.
                  Maxime unde, modi dolorum minima veritatis quos soluta.</p>
                <div class="author-date">
                  <a class="author" href="#">
                    <img src="assets/images/person1.jpg" alt="" class="rounded-circle" />
                    <span class="writer-name-small">Sallie Allen
                    </span>
                  </a>
                  <a class="date" href="#">
                    <span>21 Dec, 2019</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card border-0">
              <a href="#">
                <img src="assets/images/streets.jpg" class="card-img-top" alt="" />
              </a>
              <div class="card-body px-0">
                <ul class="category-tag-list">
                  <li class="category-tag-name">
                    <a href="#">Nature</a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="#">Beautiful streets of countryside</a>
                </h5>
                <p class="card-text pb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit
                  dolore nam ut distinctio reiciendis cupiditate, repellendus unde quis rerum, aliquid
                  esse quaerat! Vel laboriosam illo minima commodi nam, omnis dolores iusto facere.
                  Maxime unde, modi dolorum minima veritatis quos soluta.</p>
                <div class="author-date">
                  <a class="author" href="#">
                    <img src="assets/images/person1.jpg" alt="" class="rounded-circle" />
                    <span class="writer-name-small">Sallie Allen
                    </span>
                  </a>
                  <a class="date" href="#">
                    <span>21 Dec, 2019</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card border-0">
              <a href="#">
                <img src="assets/images/winter.jpg" class="card-img-top" alt="" />
              </a>

              <div class="card-body px-0">
                <ul class="category-tag-list">
                  <li class="category-tag-name">
                    <a href="#">Nature</a>
                  </li>
                </ul>
                <h5 class="card-title title-font">
                  <a href="#">Getting ready for December adventures</a>
                </h5>
                <p class="card-text pb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit
                  dolore nam ut distinctio reiciendis cupiditate, repellendus unde quis rerum, aliquid
                  esse quaerat! Vel laboriosam illo minima commodi nam, omnis dolores iusto facere.
                  Maxime unde, modi dolorum minima veritatis quos soluta.</p>
                <div class="author-date">
                  <a class="author" href="#">
                    <img src="assets/images/person1.jpg" alt="" class="rounded-circle" />
                    <span class="writer-name-small">Sallie Allen
                    </span>
                  </a>
                  <a class="date" href="#">
                    <span>21 Dec, 2019</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Archive content end -->