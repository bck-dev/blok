<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <!-- Favicon -->
  <!-- <link rel="icon" type="image/png" sizes="48x48" href="assets/images/favicon.png"> -->

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Great+Vibes&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600&display=swap" rel="stylesheet">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/blogtemplate/css/bootstrap.min.css'); ?>">	

  <!-- Fontawesome CSS-->
  <link rel="stylesheet" href="<?php echo base_url('assets/blogtemplate/css/all.css'); ?>">

  <!-- slick css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/blogtemplate/css/slick.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/blogtemplate/css/slick-theme.css'); ?>">

  <!-- Custom CSS -->
  <!-- <link rel="stylesheet" href="<?php echo base_url('assets/blogtemplate/css/preloader.css'); ?>"> -->
  <link rel="stylesheet" href="<?php echo base_url('assets/blogtemplate/css/style.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/blogtemplate/css/responsive.css'); ?>">

  <title>Blog | <?php echo $pageName; ?></title>
</head>