    <!-- Footer section -->
    <footer class="footer-section">
      <div class="container">
        <div class="footer-content-2">
          <div class="footer-logo">
            <a href="index.html">
              <h5 class="brand-name" style="font-size: 30px; font-family:'Times New Roman', Times, serif">KTMG Blog</h5>
            </a>
          </div>
          <div class="footer-links">
            <ul>
              <?php $i=1; foreach($categories as $category): ?>
                <li>
                  <a href="<?php echo base_url(); ?>category/<?php echo $category->category; ?>">
                    <?php echo $category->category; ?>
                  </a>
                </li>          
              <?php  $i++; endforeach; ?>
            </ul>
          </div>
          <div class="circular-icons social-links">
            <ul>
              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="#"><i class="fab fa-instagram"></i></a></li>
              <li><a href="#"><i class="fab fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <div class="footer-copyright">
            <p>&copy;2020 KTMG Blog. All rights reserved. Developed by <a href="#">Bckonnect</a> </p>
          </div>
        </div>
      </div>
    </footer>
    <!-- Footer section end -->
  </main>


  <!-- Scroll to top -->
  <div id="stop" class="scroll-to-top">
    <span><a href="#"><i class="fas fa-arrow-up"></i></a></span>
  </div>
  <!-- Scroll to top end -->

  <!-- Javascript --> 
  <script src="<?php echo base_url('assets/blogtemplate/js/jquery-3.4.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/blogtemplate/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/blogtemplate/js/slick.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/blogtemplate/js/jquery.sticky.js'); ?>"></script>
  <script src="<?php echo base_url('assets/blogtemplate/js/ResizeSensor.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/blogtemplate/js/theia-sticky-sidebar.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/blogtemplate/js/main.js'); ?>"></script>

</body>
</html>