<style>
    .pagination_active{
        background-color: #007bff;
        color: white;
    }
</style>

<div class="p-0 py-5 w-100 text-right">
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center p-0" id="pagination" style="font-size:20px;">
            <?php foreach ($paginationData['buttons'] as $button ):?>
                <li class="page-item">
                    <button class="page-link <?php if($button['status']=="current"): echo "pagination_active"; endif; ?>" value="<?php echo $button['id']; ?>" onclick="getData(this); updatePagination(this);">
                        <?php echo $button['value']; ?>
                    </button>
                </li>
            <?php endforeach; ?>
        </ul>
    </nav> 
</div>
<input type="hidden" id="limit" value="<?php echo $paginationData['limit'];?>" />
<input type="hidden" id="count" value="<?php echo $paginationData['count'];?>" />


<script>
    function updatePagination(id){
        var pageId = $(id).val(); 
        var limit = $('#limit').val(); 
        var count = $('#count').val(); 
        $.ajax({
            url: '<?php echo base_url().'/pagination' ?>', 
            type:'post',
            data: {current: pageId, limit: limit, count: count},
            dataType: 'json',
            success: function(results){ 
              console.log(results);
              var pagination =""; 
              jQuery.each(results.buttons, function( key, val ) {

                pagination = pagination + '<li class="page-item">';
                if(val['status']=="current"){
                    pagination = pagination + '<button class="page-link pagination_active" value="' + val['id'] + '" onclick="getData(this); updatePagination(this);">';
                }
                else{
                    pagination = pagination + '<button class="page-link" value="' + val['id'] + '" onclick="getData(this); updatePagination(this);">';
                }
                pagination = pagination + val['value']; 
                pagination = pagination + '</button>';
                pagination = pagination + '</li>';
              });
              $('#pagination').html(pagination);
            },     
            error:function(){
                console.log('error');
            }
        });
    }
</script>