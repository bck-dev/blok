<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>

<html lang="en">
	<head>

    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="author" content="Jthemes"/>	
		<meta name="description" content="Common Blog"/>
		<meta name="keywords" content="Responsive, HTML5 Template, Jthemes, One Page, Landing, Medical, Health, Healthcare, Doctor, Clinic, Care, Hospital">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
				
  		<!-- SITE TITLE -->
		<title>Blog | <?php echo $pageName; ?></title>
							
		<!-- FAVICON AND TOUCH ICONS  -->
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" sizes="152x152" href="images/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="120x120" href="images/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="76x76" href="images/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">

		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 	
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet"> 

		<!-- BOOTSTRAP CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
				
		<!-- FONT ICONS -->
		<link href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" rel="stylesheet" crossorigin="anonymous">		
        <link rel="stylesheet" href="<?php echo base_url('assets/template/css/flaticon.css'); ?>">

		<!-- PLUGINS STYLESHEET -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/css/menu.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/template/css/magnific-popup.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/template/css/owl.carousel.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/template/css/owl.theme.default.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/template/css/animate.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/template/css/jquery.datetimepicker.min.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/template/css/dropdown-effects/fade-down.css'); ?>" media="all" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url('assets/blok/custom.css'); ?>" media="all" rel="stylesheet">
				
		<!-- TEMPLATE CSS -->
		<link rel="stylesheet" href="<?php echo base_url('assets/template/css/style.css'); ?>">	
		
		<!-- RESPONSIVE CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/css/responsive.css'); ?>">
		<!-- BCK Typography -->
		<link rel="stylesheet" href="<?php echo base_url('assets/bck/css/type.css'); ?>">
		<!-- BCK spacing -->
		<link rel="stylesheet" href="<?php echo base_url('assets/bck/css/spacing.css'); ?>">
		<!-- BCK color -->
		<link rel="stylesheet" href="<?php echo base_url('assets/bck/css/color.css'); ?>">
		
		<link rel="stylesheet" href="<?php echo base_url('assets/template/css/custom.css'); ?>">
		<script src="<?php echo base_url('assets/common/js/jquery.min.js'); ?>"></script>
		<script src="https://code.jquery.com/jquery-migrate-3.1.0.min.js"></script>
	
  </head>
<body>

