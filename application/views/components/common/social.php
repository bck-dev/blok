<style>

.icon-bar {
  position: fixed;
  top: 40%;
  right: 0;
  -webkit-transform: translateY(-70%);
  -ms-transform: translateY(-70%);
  transform: translateY(-70%);
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 16px;
  transition: all 0.3s ease;
  color: white;
  font-size: 20px;
}

.icon-bar a:hover {
  background-color: #000;
}

.facebook {
  background: #3B5998;
  color: white;
}

.twitter {
  background: #55ACEE;
  color: white;
}

.google {
  background: #dd4b39;
  color: white;
}

.linkedin {
  background: #007bb5;
  color: white;
}

.youtube {
  background: #bb0000;
  color: white;
}

.content {
  margin-left: 75px;
  font-size: 30px;
}
</style>


<div class="icon-bar d-none d-lg-block">
  <a href="https://www.youtube.com/channel/UC5pMXGZ_F2OZUFdfy6YbIew" target="_blank" class="youtube"><i class="fab fa-youtube"></i></a>
  <a href="http://facebook.com/kidsandteensmedicalgroup/" target="_blank" class="facebook"><i class="fab fa-facebook-f"></i></a> 
  <a href="http://instagram.com/ktdoctor" target="_blank" class="twitter"><i class="fab fa-instagram"></i></a>
</div>
