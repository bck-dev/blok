		<!-- FOOTER-1
			============================================= -->
			<footer id="footer-1" class="bg-image wide-40 footer division">
				<div class="container">


					<!-- FOOTER CONTENT -->
					<div class="row">	


						<!-- FOOTER INFO -->
						<div class="col-md-6 col-lg-3">
							<div class="footer-info mb-40">

								<!-- Footer Logo -->
								<!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 360 x 80  pixels) -->
								<h5 class="h5-xs">Urgent Care</h5>

								<!-- Text -->	
								<p class="p-sm mt-20">
									We are considered as one of the most visionary Pediatric Practices in the country. 
									We believe in delivering the best, most up to date care for your children. 
									As children of the future, they deserve it.
								</p>  

								<!-- Social Icons -->
								<div class="footer-socials-links mt-20">
									<ul class="foo-socials text-center clearfix">

										<li><a href="https://www.facebook.com/pediatricafterhoursusa/" class="ico-facebook" target="_blank"><i class="fab fa-facebook-f"></i></a></li>												
										<li><a href="https://www.instagram.com/napediatricurgentcare/" class="ico-instagram" target="_blank"><i class="fab fa-instagram"></i></a></li>									
										<li><a href="https://www.youtube.com/channel/UC5pMXGZ_F2OZUFdfy6YbIew" class="ico-youtube" target="_blank"><i class="fab fa-youtube"></i></a></li>			
																																				
										<!--
										<li><a href="#" class="ico-behance"><i class="fab fa-behance"></i></a></li>	
										<li><a href="#" class="ico-dribbble"><i class="fab fa-dribbble"></i></a></li>
										<li><a href="#" class="ico-linkedin"><i class="fab fa-linkedin-in"></i></a></li>
										<li><a href="#" class="ico-pinterest"><i class="fab fa-pinterest-p"></i></a></li>										
										<li><a href="#" class="ico-vk"><i class="fab fa-vk"></i></a></li>
										<li><a href="#" class="ico-yelp"><i class="fab fa-yelp"></i></a></li>
										<li><a href="#" class="ico-yahoo"><i class="fab fa-yahoo"></i></a></li>
									    -->	

									</ul>									
								</div>	
							
							</div>		
						</div>


						<!-- FOOTER CONTACTS -->
						<div class="col-md-6 col-lg-3">
							<div class="footer-box mb-40">
							
								<!-- Title -->
								<h5 class="h5-xs">Our Location</h5>

								<!-- Address -->
								<p>504 S. Sierra Madre Blvd,</p> 
								<p>Pasadena, CA 91107</p>
								
								<!-- Email -->
								<p class="foo-email mt-20">E: <a href="mailto:info@ktdoctor.com" class="blue-color">info@ktdoctor.com</a></p>

								<!-- Phone -->
							
								 <p class="foo-email">P: <a href="tel:8183615437" class="blue-color">(818) 361-5437</a></p>

							</div>
						</div>


						<!-- FOOTER WORKING HOURS -->
						<div class="col-md-6 col-lg-3">
							<div class="footer-box mb-40">
							
								<!-- Title -->
								<h5 class="h5-xs">Quick Access</h5>

								<!-- Working Hours -->
								<p class="footer-link"><a href="<?php echo base_url('locations'); ?>">Locations</a></p>
								<p class="footer-link"><a href="https://www.ktdoctor.com/doctors">Our Doctors</a></p>
								<p class="footer-link"><a href="<?php echo base_url('contact'); ?>">Contact Us</a></p>
								<p class="footer-link"><a href="<?php echo base_url('insurance'); ?>">Insurance</a></p>									

							</div>
						</div>


						<!-- FOOTER PHONE NUMBER -->
						<div class="col-md-6 col-lg-3">
							<div class="footer-box mb-40">
												
								<!-- Title -->
								<h5 class="h5-xs">Text for Appointment</h5>

								<!-- Footer List -->
								<p class="p-sm mt-15 white">ENGLISH TEXT</p>
								<a href="sms:6262987121" >
									<h5 class="h5-xl blue-color">(626) 298-7121</h5>
								</a>

								<p class="p-sm mt-15 white">SPANISH TEXT</p>
								<a href="sms:6262697744" >
									<h5 class="h5-xl blue-color">(626) 269-7744</h5>
								</a>

							</div>
						</div>	


					</div>	  <!-- END FOOTER CONTENT -->


					<!-- FOOTER COPYRIGHT -->
					<div class="bottom-footer">
						<div class="row">
							<div class="col-md-12">
								<p class="footer-copyright">&copy; 2020 <span>Urgent Care</span>. All Rights Reserved</p>
							</div>
						</div>
					</div>


				</div>	   <!-- End container -->										
			</footer>	<!-- END FOOTER-1 -->




		</div>	<!-- END PAGE CONTENT -->
		



		<!-- EXTERNAL SCRIPTS
		============================================= -->	
		
		
		
        <script src="<?php echo base_url('assets/template/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/modernizr.custom.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/jquery.easing.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/menu.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/owl.carousel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/jquery.datetimepicker.full.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/hero-form.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/imagesloaded.pkgd.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/jquery.appear.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/jquery.stellar.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/sticky.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/jquery.scrollto.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/materialize.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/jquery.magnific-popup.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/isotope.pkgd.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/contact-form.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/comment-form.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/appointment-form.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/jquery.ajaxchimp.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/wow.js'); ?>"></script>
        <script src="<?php echo base_url('assets/template/js/custom.js'); ?>"></script>

		<script> 
			new WOW().init();
		</script>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
		<!-- [if lt IE 9]>
			<script src="js/html5shiv.js" type="text/javascript"></script>
			<script src="js/respond.min.js" type="text/javascript"></script>
		<![endif] -->

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->	
		<!--
		<script>
			window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
			ga('create', 'UA-XXXXX-Y', 'auto');
			ga('send', 'pageview');
		</script>
		<script async src='https://www.google-analytics.com/analytics.js'></script>
		-->
		<!-- End Google Analytics -->

	</body>


</html>	