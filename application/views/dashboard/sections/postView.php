<?php $this->load->view('dashboard/common/header.php')?>
<?php $this->load->view('dashboard/common/sidebar.php')?>

<div class="content-wrapper p-3">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">View post</h3>
    </div>
    <div class="card-body">            
                    <p style="text-align: center"><b>Title: </b><?php echo $allData->title ?></p>
                    <p style="text-align: center"><b>Feature Image: </b><br><img src="<?php echo base_url(); ?>/uploads/<?php echo $allData->featureImage; ?>" width="50%"/></p>
                    <p><b>Content: </b><br><?php echo $allData->content ?></p>
                    <p><b>Short Description: </b><?php echo $allData->shortDescription ?></p>
                    <div class="row">
                      <div class="col-sm"><b>Category: </b><?php echo $allData->categoryName ?></div>
                      <div class="col-sm"> <b>Blog For: </b><?php echo $allData->blogFor ?></div>
                    </div><br>
                    <p><b>Tags: </b>
                    <?php foreach($tagData as $tag): ?>
                      <?php echo $tag->tagName; ?>
                    <?php endforeach; ?></p>
    </div>
</div>
</div>



<?php $this->load->view('dashboard/common/footer.php')?>