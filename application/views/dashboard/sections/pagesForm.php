<div class="row">
  <div class="col-6" .mt-2>
    <div class="content-wrapper p-3">
      <?php $this->load->view('dashboard/sections/error') ?>
      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-lg-12">
            <!-- general form elements -->
            <div class="card card-primary">
            <div class="card-header">
             <h3 class="card-title">Pages Form</h3>
            </div>         
            <!-- form start -->
            <form action="<?php echo base_url('admin/pages/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="addForm">
              <div class="card-body">
                <div class="form-group">
                  <label for="inputTitle">Page Name</label>
                  <input type="text" class="form-control" placeholder="Enter Page Name" name='name' value="<?php echo $updateData->name; ?>" >
                </div>
              <!-- /.card-body -->       
              <?php  if($action == 'update') { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                </div>
                <?php }else { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                </div>
              <?php } ?>                
            </form>
          </div>
        </div>        
      </section>
    </div>
  </div>

