<?php $this->load->view('dashboard/common/header.php')?>
<?php $this->load->view('dashboard/common/sidebar.php')?>

<div class="content-wrapper p-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Add new post</h3>
        </div>
        <div class="card-body">            
        <form action="<?php echo base_url('admin/post/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="postForm" enctype='multipart/form-data' >
                <div class="card-body">
                   
                    <div class="form-group">
                          <label>Title</label>
                          <input type="text" class="form-control" placeholder="Enter Title" name='title' value="<?php echo $updateData->title; ?>" required>
                    </div>

                    <div class="form-group">
                          <label>Content</label>
                          <textarea class="form-control textarea" rows="30" placeholder="Enter Content" name='content' required><?php echo $updateData->content; ?></textarea>
                    </div>

                    <div class="form-group">
                          <label>Short Description</label>
                          <textarea class="form-control textarea" rows="15" placeholder="Enter Short Description" name='shortDescription' required><?php echo $updateData->shortDescription; ?></textarea>
                    </div>

                    <div class="form-group">
                      <label>Feature Image</label>
                      <?php  if($action == 'update') { ?>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <img src="<?php echo base_url(); ?>/uploads/<?php echo $updateData->featureImage;?>" width="250"/>
                        </div>
                      </div>
                      <?php }?>  
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input"  name="featureImage" size="300"  <?php  if($action != 'update'): echo 'required'; endif;?>>
                          <label class="custom-file-label">Choose file</label>
                        </div>
                      </div>
                    </div>

                  <div class="form-group">
                    <div class="col-sm-12">
                      <label>Category</label>                  
                      <select class="custom-select" name='category' required >
                        <?php if($action=="update"): ?>
                          <option value="<?php echo $updateData->categoryId; ?>" selected="selected"><?php echo $updateData->categoryName; ?></option>
                        <?php foreach($nonSelectedCategories as $category): ?>
                          <option value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                        <?php endforeach; ?>  
                        <?php else: ?>
                          <option value="" disabled="disabled" selected="selected">Select Category</option>
                        <?php foreach($categories as $category): ?>
                          <option value="<?php echo $category->id; ?>"><?php echo $category->category; ?></option>
                        <?php endforeach; ?>
                        <?php endif; ?>    
                      </select> 
                    </div>                         
                  </div>

                  <div class="form-group">
                    <div class="col-sm-12">
                      <label>Blog For</label>                  
                      <select class="custom-select" name='blogFor' required>
                        <?php if($action=="update"): ?>
                          <option value="<?php echo $updateData->blogFor; ?>"><?php echo strtoupper($updateData->blogFor); ?></option>
                        <?php else: ?>
                          <option value="" disabled="disabled" selected="selected">Select Website</option>
                        <?php endif; ?>
                          <option value="ktd">KTD</option>
                          <option value="uc">UC</option>
                          <option value="serendib">Serendib</option>
                      </select> 
                      </div>                         
                  </div>  

                  <div class="form-group">
                        <div class="col-sm-12">
                          <label>Tags</label>                  
                          <div class="select2-purple">
                            <select class="select2" name='tags[]' multiple="multiple" style="width:100%;" data-placeholder="Select Tags" >
                              <?php if($action=="update"): ?>
                                <?php foreach($tagData as $tag): ?>
                                  <option value="<?php echo $tag->id; ?>" selected=""><?php echo $tag->tagName; ?></option>
                                <?php endforeach; ?>
                                <?php foreach($nonSelectedTags as $tag): ?>
                                  <option value="<?php echo $tag->id; ?>"><?php echo $tag->tagName; ?></option>
                                <?php endforeach; ?>  
                              <?php else: ?>      
                                <?php foreach($tags as $tag): ?>
                                  <option value="<?php echo $tag->id; ?>"><?php echo $tag->tagName; ?></option>
                                <?php endforeach; ?>   
                              <?php endif; ?>     
                            </select> 
                          </div>
                        </div>                         
                    </div>

                </div>
                                             
                <?php  if($action == 'update'): ?>
                  <div class="card-footer">
                    <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                  </div>
                <?php else : ?>                    
                  <div class="card-footer">
                    <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                  </div>
                <?php endif; ?>
                            
              </form>
        </div>
    </div>
</div>



<?php $this->load->view('dashboard/common/footer.php')?>