<div class="content-wrapper p-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Posts <a href="<?php echo base_url('admin/post/newpost');?>" class="btn btn-sm btn-success ml-4">Add new post</a> </h3>
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Feature Image</th>
                <th>Category</th>
                <th>Blog For</th>               
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                <?php $i=1; foreach($allData as $dataRow): ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $dataRow->title; ?></td>
                        <td><img src="<?php echo base_url(); ?>/uploads/<?php echo $dataRow->featureImage; ?>" width="100"/></td>
                        <td><?php echo $dataRow->categoryName; ?></td>
                        <td><?php echo strtoupper($dataRow->blogFor); ?></td>
                        <td>
                            <a class="btn btn-xs btn-success" href="<?php echo base_url();?>/admin/post/viewpost/<?php echo $dataRow->id ?>">view</a>
                            <a class="btn btn-xs btn-warning" href="<?php echo base_url();?>/admin/post/loadupdate/<?php echo $dataRow->id ?>">Edit</a>
                            <a class="btn btn-xs btn-danger" href="<?php echo base_url();?>/admin/post/delete/<?php echo $dataRow->id ?>">Delete</a>
                        </td>
                    </tr>
                <?php $i++; endforeach; ?>
            </tfoot>
            </table>
        </div>
    </div>
</div>

