<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['home'] = 'home';
$route['home/(:any)'] = 'home/redirectToHome/$1';
$route['posts'] = 'home/posts';
$route['results'] = 'home/results';
$route['viewpost/(:num)'] = 'home/viewPost/$1';
$route['category'] = 'home/category';
$route['category/(:any)'] = 'home/categoryPosts/$1';
$route['tag/(:any)'] = 'home/tagPosts/$1';
$route['authors'] = 'home/authors';
$route['author/(:any)'] = 'home/authorPosts/$1';
$route['months'] = 'home/months';
$route['monthposts/(:any)'] = 'home/monthPosts/$1';
$route['search'] = 'home/search';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//login
$route['login'] = 'admin/login';
$route['admin'] = 'admin/login';
$route['login/check'] = 'admin/login/checkLogin';
$route['logout'] = 'admin/user/logout';

//user
$route['admin/user'] = 'admin/user';
$route['admin/user/delete/(:num)'] = 'admin/user/delete/$1';
$route['admin/user/loadupdate/(:num)'] = 'admin/user/loadUpdate/$1';
$route['admin/user/update/(:num)'] = 'admin/user/update/$1';

//pages
$route['admin/pages'] = 'admin/pages';
$route['admin/pages/delete/(:num)'] = 'admin/pages/delete/$1';
$route['admin/pages/loadupdate/(:num)'] = 'admin/pages/loadUpdate/$1';
$route['admin/pages/update/(:num)'] = 'admin/pages/update/$1';

//category
$route['admin/category'] = 'admin/category';
$route['admin/category/delete/(:num)'] = 'admin/category/delete/$1';
$route['admin/category/loadupdate/(:num)'] = 'admin/category/loadUpdate/$1';
$route['admin/category/update/(:num)'] = 'admin/category/update/$1';

//tag
$route['admin/tag'] = 'admin/tag';
$route['admin/tag/delete/(:num)'] = 'admin/tag/delete/$1';
$route['admin/tag/loadupdate/(:num)'] = 'admin/tag/loadUpdate/$1';
$route['admin/tag/update/(:num)'] = 'admin/tag/update/$1';

//post
$route['admin/post'] = 'admin/post';
$route['admin/post/newpost'] = 'admin/post/newPost';
$route['admin/post/viewpost/(:num)'] = 'admin/post/viewPost/$1';
$route['admin/post/delete/(:num)'] = 'admin/post/delete/$1';
$route['admin/post/loadupdate/(:num)'] = 'admin/post/loadUpdate/$1';
$route['admin/post/update/(:num)'] = 'admin/post/update/$1';

//api
$route['pagination'] = 'home/pagination';