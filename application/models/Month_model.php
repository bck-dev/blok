<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Month_model extends CI_Model
{
    public function getMonths($blogFor=null) {
        $this->db->select("DISTINCT (DATE_FORMAT(date,'%Y-%m')) as month");
        $this->db->from('posts');
        if($blogFor != null){
            $this->db->where('blogFor', $blogFor);
        }
        return $this->db->get()->result();
    }
}