<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model
{
    public function nonSelectedCategories($id)
     {
        $this->db->select('categories.id AS categoryId, categories.category AS categoryName');
        $this->db->from('categories');
        $this->db->where_not_in('id', $id);
        return $this->db->get()->result();
    }

    public function getCategory($id)
    {
        $this->db->select('categories.category AS categoryName');
        $this->db->from('categories');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function getAllCategories()
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->order_by('category');
        return $this->db->get()->result();
    }   

    public function searchCategory($key,$blogFor=null)
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->like('category', $key);
        $result =  $this->db->get()->row();

        if($result->id != NULL)
        {
            $this->db->select('*');
            $this->db->from('posts');
            $this->db->where('category', $result->id);
            if($blogFor != null){
             $this->db->where('blogFor', $blogFor);
            }
            return $this->db->get()->result();
        }else{
            return null;
        }
     
    }

}