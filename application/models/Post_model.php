<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model
{
    public function getAllPosts() {
        $this->db->select("posts.*,categories.category AS 'categoryName'");
        $this->db->from('posts');
        $this->db->join('categories', 'posts.category = categories.id');
        return $this->db->get()->result();
    }

    public function searchResult($id) {
        $this->db->select("*");
        $this->db->from('posts');
        $this->db->where('id',$id);
        return $this->db->get()->row();
    }

    public function getPostById($id) {
        $this->db->select("posts.*,categories.category AS 'categoryName',categories.id AS 'categoryId'");
        $this->db->from('posts');
        $this->db->where('posts.id',$id);
        $this->db->join('categories', 'posts.category = categories.id');
        return $this->db->get()->row();
    }

    public function getTags($id) {
        $this->db->select('tags.tagName,tags.id');
        $this->db->from('post_has_tags');
        $this->db->where('post_has_tags.postId',$id);
        $this->db->join('tags', 'tags.id = post_has_tags.tagId');
        return $this->db->get()->result();
    }

    public function nonSelectedTags($id) {
        $this->db->select('*');
        $this->db->from('post_has_tags');
        $this->db->where('postId', $id);
        $data = $this->db->get()->result();

        foreach($data as $d){
            $tagIds[] = $d->tagId;
        }

        $this->db->select('tags.id, tags.tagName');
        $this->db->from('tags');
        $this->db->where_not_in('id', $tagIds);
        return $this->db->get()->result();
    }

    public function getPaginationPosts($limit, $start , $blogFor){
        $this->db->limit($limit, $start);
        $this->db->where('blogFor', $blogFor);
        return $this->db->get('posts')->result();
    }

    public function getAllPaginationPosts($limit, $start){
        $this->db->limit($limit, $start);
        $query = $this->db->get('posts');
        return $query->result();
    }

    public function getAllCount() {
        return $this->db->count_all('posts');
    }

    public function getCount($blogFor) {
        $this->db->like('blogFor', $blogFor);
        $this->db->from('posts');
        return $this->db->count_all_results();
    }

    // public function searchPost($key){
    //     $this->db->select('*');
    //     $this->db->from('posts');
    //     $this->db->like('title', $key);
    //     $this->db->or_like('author', $key);
    //     $this->db->or_like('content', $key);
    //     return $this->db->get()->result();
    // }

    public function searchPost($key,$blogFor=null){
        
        if($blogFor != null){
            $where = "blogFor= '$blogFor' AND (content LIKE '%$key%' OR title LIKE '%$key%' OR author LIKE '%$key%')";
        }else{
            $where = "content LIKE '%$key%' OR title LIKE '%$key%' OR author LIKE '%$key%'";
        }

        $this->db->select('*');
        $this->db->from('posts');
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function getAuthorPosts($author,$blogFor=null){
        $this->db->from('posts');
        $this->db->where('author',$author);
        if($blogFor!=null){
            $this->db->where('blogFor',$blogFor);
        }
        return $this->db->get()->result();
    }

    public function getAuthorPostsCount($author,$blogFor=null){
        $this->db->from('posts');
        $this->db->where('author',$author);
        if($blogFor!=null){
            $this->db->where('blogFor',$blogFor);
        }
        return $this->db->count_all_results();
    }    

    public function getCategoryPosts($category,$blogFor=null){
        $this->db->from('posts');
        $this->db->where('category',$category);
        if($blogFor!=null){
            $this->db->where('blogFor',$blogFor);
        }
        
        $postData = $this->db->get()->result();

        foreach($postData as $post){			
			$cat = $this->category_model->getCategory($post->category);
			$post->categoryName = $cat->categoryName;
        }

        return $postData;
    }

    public function getTagPosts($tag, $blogFor=null){
        $this->db->select('*');
        $this->db->from('post_has_tags');
        $this->db->where('tagId', $tag);
        $postIds =  $this->db->get()->result();

        $postData = [];
        $results = [];
        $i=0;
        foreach($postIds as $pid){
            $this->db->select('*');
            $this->db->from('posts');
            $this->db->where('id', $pid->postId);
            if($blogFor != null){
                $this->db->where('blogFor', $blogFor);
            }
            $results[$i] = $this->db->get()->row();
            if($results[$i] != null){
            $postData[$i] = $results[$i];
            }
            $i++;
        }

        foreach($postData as $post){			
			$cat = $this->category_model->getCategory($post->category);
			$post->categoryName = $cat->categoryName;
        }
        
        return $postData;
    }

    public function getCategoryPostsCount($category,$blogFor=null){
        $this->db->from('posts');
        $this->db->where('category',$category);
        if($blogFor!=null){
            $this->db->where('blogFor',$blogFor);
        }
        return $this->db->count_all_results();
    } 

    
    public function getOrderedPosts($orderby,$blogFor=null){
        $this->db->from('posts');
        $this->db->order_by($orderby, "desc");
        if($blogFor!=null){
            $this->db->where('blogFor',$blogFor);
        }
        $this->db->limit(4);
        return $this->db->get()->result();
    } 


}