<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model
{
    public function getAllData($table) {
        return $this->db->get($table)->result();
    }

    public function getAllDataWithLimit($table) {
        $this->db->from($table);
        $this->db->order_by('id', 'DESC');
       // $this->db->limit('10');
        return $this->db->get()->result();
    }    

    public function getById($table, $id) {
        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function count($table, $field, $value) {
        $this->db->from($table);
        $this->db->where($field, $value);
        return $this->db->count_all_results();
    }

    public function getByFeild($table, $field, $value) {
        $this->db->from($table);
        $this->db->where($field, $value);
        return $this->db->get()->result();
    }

    public function getByCondition($table, $where) {
        $this->db->from($table);
        $this->db->where($where);
        return $this->db->get()->result();
    }

    public function countCondition($table, $where) {
        $this->db->from($table);
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function delete($table, $id) {
        $this->db->where('id', $id);
        $this->db->delete($table);
    }

    public function deleteByFeild($table, $field, $value) {
        $this->db->where($field, $value);
        $this->db->delete($table);
    }

    public function update($table, $id, $data) {
        $this->db->where('id', $id);
        $this->db->update( $table, $data);
    }

    public function updateMultipleCondition($table, $conditions, $data) {
        $conditionCount=count($conditions);
        $keys=array_keys($conditions);
        $values=array_values($conditions);
        
        $i=0;
        while($i<$conditionCount){
            $this->db->where($keys[$i], $values[$i]);
            $i++;
        }
        
        $this->db->update( $table, $data);
    }

    public function loadTemplateData($pageName){
        
        $categories = $this->category_model->getAllCategories();
		foreach($categories as $category){
			$category->count = $this->post_model->getCategoryPostsCount($category->id, $_SESSION['blogFor']);
        };

        $tags = $this->tag_model->tagsWithCount($_SESSION['blogFor']);
        
        $data=[ 'pageName' => $pageName, 
                'categories' => $categories,
                'tags' => $tags
        ];

        return $data;
    }

    public function distinctField(){
        $this->db->from('posts');
		$this->db->distinct("DATE_FORMAT(date,'%m-%Y')");
		return $this->db->get()->result();
    }

    public function pagination($count, $limit, $current){
        
        if($count != 0){
            $pageCount = number_format(ceil($count/$limit));
            $paginationButtons = [];

            $max= $pageCount>$current+2 ? $current+2 : $pageCount;
            $x= $current-2>0 ? $current-2 : $x=1;  
                    
            if($current==1){
                $i=0;
            }
            else{
                $paginationButtons[0]['value'] = "Previous"; 
                $paginationButtons[0]['id'] = $current-1; 
                $i=1;
            }

            if($current-2>=2){
                $paginationButtons[$i]['value'] = "First: 1"; 
                $paginationButtons[$i]['id'] = 1;  
                $paginationButtons[$i]['status'] = "first"; 
                $i++;
            }

            while($x<=$max){
                if($x>=$current-2){
                    $paginationButtons[$i]['value'] = $x; 
                    $paginationButtons[$i]['id'] = $x; 
                    if($x==$current){
                        $paginationButtons[$i]['status'] = "current"; 
                    }
                    $i++;
                }
                $x++;
            }

            if($current+2<$pageCount){
                $paginationButtons[$i]['value'] = "Last: ".$pageCount; 
                $paginationButtons[$i]['id'] = $pageCount;             
                $paginationButtons[$i]['status'] = "last"; 
                $i++;
            }

            if($pageCount!=$current){
                $paginationButtons[$i+1]['value'] = "Next"; 
                $paginationButtons[$i+1]['id'] = $current+1; 
            }

            return $paginationButtons;
        }
    }

}