<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Tag_model extends CI_Model
{
    public function searchTag($key,$blogFor=null)
    {
        $this->db->select('*');
        $this->db->from('tags');
        $this->db->like('tagName', $key);
        $result =  $this->db->get()->row();

        if($result->id != NULL)
        {
            $this->db->select('*');
            $this->db->from('post_has_tags');
            $this->db->where('tagId', $result->id);
            $postIds =  $this->db->get()->result();

            $postData = [];
            $results = [];
            $i=0;
            foreach($postIds as $pid){
                $this->db->select('*');
                $this->db->from('posts');
                $this->db->where('id', $pid->postId);
                if($blogFor != null){
                    $this->db->where('blogFor', $blogFor);
                }
                $results[$i] = $this->db->get()->row();
                if($results[$i] != null){
                $postData[$i] = $results[$i];
                }
                $i++;
            }
             return $postData;
        }else{
            return null;
        }
       
    }
    public function tagsWithCount($blogFor=null){
        $this->db->select('*');
        $this->db->from('tags');
        $this->db->order_by('tagName');
        $tags = $this->db->get()->result();

        foreach ($tags as $tag) {
            $this->db->from('post_has_tags');
            $this->db->where('tagId', $tag->id);

            if($blogFor==null){                
                $tag->count = $this->db->count_all_results();
            }else{
                $postIds = $this->db->get()->result();
                $count = 0;
                foreach ($postIds as $pId){
                    $this->db->from('posts');
                    $this->db->where('id', $pId->postId);
                    $this->db->where('blogFor',$blogFor);
                    if($this->db->count_all_results()>0){
                        $count = $count+1;
                    }
                }

                $tag->count = $count;
            }
            
        }

        return $tags;
    }

    public function getTagOfPost($id){
        $this->db->select('tagId');
        $this->db->from('post_has_tags');
        $this->db->where('postId', $id);        
        $tagIds = $this->db->get()->result();
        $tags = [];
        $i=0;
        foreach ($tagIds as $tagId) {
            $this->db->select('tagName');
            $this->db->from('tags');
            $this->db->where('id', $tagId->tagId);
            $tags[$i] = $this->db->get()->row();
            $i++;
        }

        return $tags;
    }
}