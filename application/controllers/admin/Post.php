<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Post extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
  $allData = $this->post_model->getAllPosts();
  $categories = $this->common_model->getAllData('categories');
  $tags = $this->common_model->getAllData('tags');

  $data=['pageName'=>"Post",
         'allData' => $allData,
         'categories' => $categories,
         'tags' => $tags
        ];

  $this->load->view('dashboard/post',$data);
}

public function newPost()
{
  $categories = $this->common_model->getAllData('categories');
  $tags = $this->common_model->getAllData('tags');

  $data=['pageName'=>"Post",
          'categories' => $categories,
          'tags' => $tags,
          'action'  => 'add'
        ];
  $this->load->view('dashboard/sections/postForm', $data);
}

public function add()
{
  $config['upload_path']   = './uploads/';
  $config['allowed_types'] = 'jpg|png|jpeg';
  $config['max_size']      = 5000;
  $config['encrypt_name']  = TRUE;
  
  $this->load->library('upload', $config);
  $this->upload->do_upload('featureImage');
  $upload_data = $this->upload->data();
  $image_name = $upload_data['file_name'];

  $userName = $this->session->userdata('name');

  $data = array
  (
    'title' => $_POST['title'],
    'content' =>$_POST['content'],
    'shortDescription' => $_POST['shortDescription'],
    'featureImage' =>$image_name,  
    'category' =>$_POST['category'],
    'blogFor' => $_POST['blogFor'],
    'date' => date("Y-m-d"),
    'author' => $userName
  );      
  
  $this->db->insert('posts', $data);
    
  $this->db->select_max('id');                                                                   
  $query = $this->db->get('posts');    
    
  foreach ($query->result() as $row)
  {
    $postId = $row->id;
  }

  foreach($_POST['tags'] as $selectedTag)
  {
    $tagData = array(
      'tagId' => $selectedTag,
      'postId' => $postId
    );

  $this->db->insert('post_has_tags', $tagData);
  }

  $categories = $this->common_model->getAllData('categories');
  $tags = $this->common_model->getAllData('tags');

  $allData = $this->post_model->getAllPosts();

  $data=['pageName'=>"Post",
          'allData' => $allData, 
          'categories' => $categories,
          'tags' => $tags,
          'check' => 'success'
        ];

  $this->load->view('dashboard/post',$data);

}

public function delete($id){

  $this->common_model->delete('posts', $id);
  $this->common_model->deleteByFeild('post_has_tags', 'postId',$id);

  $categories = $this->common_model->getAllData('categories');
  $tags = $this->common_model->getAllData('tags');

  $allData = $this->post_model->getAllPosts();

  $data=['pageName'=>"Post",
          'allData' => $allData, 
          'categories' => $categories,
          'tags' => $tags
        ];
  $this->load->view('dashboard/post', $data); 

}

public function loadUpdate($id){

  $updateData = $this->post_model->getPostById($id);
  $tagData = $this->post_model->getTags($id);
  $categories = $this->common_model->getAllData('categories');
  $tags = $this->common_model->getAllData('tags');
  $nonSelectedTags = $this->post_model->nonSelectedTags($id);
  $nonSelectedCategories = $this->category_model->nonSelectedCategories($updateData->category);

  $data=['pageName'=>"Post",
        'updateData'  => $updateData,
        'action'  => 'update',
        'tagData' => $tagData,
        'categories' => $categories,
        'tags' => $tags,
        'nonSelectedTags' => $nonSelectedTags,
        'nonSelectedCategories' => $nonSelectedCategories
        ];

   $this->load->view('dashboard/sections/postForm', $data);

}

public function update($id){
  
  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'jpg|png|jpeg';
  $config['max_size']             = 5000;
  $config['encrypt_name']         = TRUE;

  $this->load->library('upload', $config);
  $this->upload->do_upload('featureImage');
  $uploadData = $this->upload->data();
 
  if($uploadData['file_path']!=$uploadData['full_path']){
    
    $data = array
    (
      'title' => $_POST['title'],
      'content' =>$_POST['content'],
      'shortDescription' => $_POST['shortDescription'],
      'featureImage' =>$uploadData['file_name'],
      'category' =>$_POST['category'],
      'blogFor' => $_POST['blogFor']
    ); 
    
  }
  else{
    $data = array
    (
      'title' => $_POST['title'],
      'content' =>$_POST['content'],
      'shortDescription' => $_POST['shortDescription'],
      'category' =>$_POST['category'],
      'blogFor' => $_POST['blogFor']
    ); 
  }

  $this->common_model->update('posts', $id, $data);

  $this->common_model->deleteByFeild('post_has_tags', 'postId', $id);

  foreach($_POST['tags'] as $selectedTag){
    $tagData = array(
      'tagId' => $selectedTag,
      'postId' => $id
    );

  $this->db->insert('post_has_tags', $tagData);
  }

  $allData = $this->post_model->getAllPosts();

  $data=['pageName'=>"Post",
          'allData' => $allData
        ];

  $this->load->view('dashboard/post', $data);

}

public function viewPost($id)
{
  $allData = $this->post_model->getPostById($id);
  $categories = $this->common_model->getAllData('categories');
  $tagData = $this->post_model->getTags($id);

  $data=['pageName'=>"Post",
          'allData' => $allData, 
          'categories' => $categories,
          'tagData' => $tagData
        ];

  $this->load->view('dashboard/sections/postView', $data);
}


} 

?>