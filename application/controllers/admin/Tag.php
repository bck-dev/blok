<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Tag extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
  $allData = $this->common_model->getAllData('tags');

  $data=['pageName'=>"Tag",
          'action'  => 'add',
          'allData' => $allData 
        ];

  $this->load->view('dashboard/tag',$data);

}

public function add()
{
  $data = array
  (
    'tagName' => $_POST['tagName'],
    'slug' => $_POST['slug']
  );  

  $this->db->insert('tags', $data);
  $allData = $this->common_model->getAllData('tags');

  $data=['pageName'=>"Tag",
          'action'  => 'add',
          'allData' => $allData 
        ];
          
  $this->load->view('dashboard/tag',$data);
}

public function delete($id){

  $this->common_model->delete('tags', $id);
  $allData = $this->common_model->getAllData('tags');

  $data=['pageName'=>"Tag",
          'allData' => $allData, 
          'action'  => 'add'
        ];
      
  $this->load->view('dashboard/tag', $data); 
}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('tags',$id);
  $allData = $this->common_model->getAllData('tags');
 
  $data=['pageName'=>"Tag",
          'allData' => $allData, 
          'updateData'  => $updateData,
          'action'  => 'update'
        ];

  $this->load->view('dashboard/tag', $data);
}

public function update($id){
    
  $data = array
  (
    'tagName' => $_POST['tagName'],
    'slug' => $_POST['slug']
  ); 

  $this->common_model->update('tags', $id, $data);
  $allData = $this->common_model->getAllData('tags');

  $data=['pageName'=>"Tag",
          'allData' => $allData,
          'action'  => 'add'
        ];

  $this->load->view('dashboard/tag', $data);

}

} 

?>