<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Category extends BaseController {

public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
  $allData = $this->common_model->getAllData('categories');

  $data=['pageName'=>"Category",
          'action'  => 'add',
          'allData' => $allData 
        ];

  $this->load->view('dashboard/category',$data);

}

public function add()
{
  $data = array
  (
    'category' => $_POST['category'],
    'slug' => $_POST['slug']
  );  

  $this->db->insert('categories', $data);
  $allData = $this->common_model->getAllData('categories');

  $data=['pageName'=>"Category",
          'action'  => 'add',
          'allData' => $allData 
        ];
          
  $this->load->view('dashboard/category',$data);

}

public function delete($id){

  $this->common_model->delete('categories', $id);
  $allData = $this->common_model->getAllData('categories');

  $data=['pageName'=>"Category",
          'allData' => $allData, 
          'action'  => 'add'
        ];
      
  $this->load->view('dashboard/category', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('categories',$id);
  $allData = $this->common_model->getAllData('categories');
 
  $data=['pageName'=>"Category",
          'allData' => $allData, 
          'updateData'  => $updateData,
          'action'  => 'update'
        ];

  $this->load->view('dashboard/category', $data);

}

public function update($id){
    
  $data = array
  (
    'category' => $_POST['category'],
    'slug' => $_POST['slug']
  ); 

  $this->common_model->update('categories', $id, $data);
  $allData = $this->common_model->getAllData('categories');

  $data=['pageName'=>"Category",
          'allData' => $allData,
          'action'  => 'add'
        ];

  $this->load->view('dashboard/category', $data);

}

} 

?>