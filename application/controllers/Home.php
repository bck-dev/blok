<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class Home extends CI_Controller {

	public function index()
	{			
		if(!isset($_SESSION['blogFor'])){
			$_SESSION['blogFor'] = null;
		}

		$limit = 12;
		$pageName="Home";

		$postData = $this->post_model->getAllPaginationPosts($limit, 0);
		$count = $this->post_model->getAllCount();

		$paginationData['buttons'] = $this->common_model->pagination($count, $limit, 1);
		$paginationData['count']   = $count;
		$paginationData['limit']   = $limit;

		$sliderPosts = $this->tag_model->searchTag('slider', $_SESSION['blogFor']);
		foreach($sliderPosts as $post){
			$post->categoryName = $this->category_model->getCategory($post->category);
		}
		$featuredPosts = $this->tag_model->searchTag('featured',$_SESSION['blogFor']);
		foreach($featuredPosts as $post){
			$post->categoryName = $this->category_model->getCategory($post->category);
		}
		$popularPosts = $this->post_model->getOrderedPosts('viewCount',$_SESSION['blogFor']);
		foreach($popularPosts as $post){
			$post->categoryName = $this->category_model->getCategory($post->category);
		}
		$recentPosts = $this->post_model->getOrderedPosts('id',$_SESSION['blogFor']);
		foreach($recentPosts as $post){
			$post->categoryName = $this->category_model->getCategory($post->category);
		}

		$categories = $this->category_model->getAllCategories();

		$data=['pageName'=>$pageName, 
			   'postData'=> $postData,
			   'paginationData' => $paginationData,
			   'sliderPosts' => $sliderPosts,
			   'featuredPosts' => $featuredPosts,
			   'popularPosts' => $popularPosts,
			   'recentPosts' => $recentPosts,
			   'categories' => $categories];

		$this->load->view('home', $data);
	}

	public function redirectToHome($blogFor)
	{		
		if($blogFor=="SERENDIB" OR $blogFor=="KTD" OR $blogFor=="UC" ){
			$_SESSION['blogFor'] = $blogFor;
		}else{
			$_SESSION['blogFor'] = null;
		}

		$this->index();
	}	

	public function posts()
	{		
		$id = $_POST['id'];		
		$limit = 12;
		$start = ($id*$limit) - ($limit);
		
		if(!isset($_SESSION['pagename'])) {
			$postData = $this->post_model->getAllPaginationPosts($limit, $start );	
		}else{
			$postData = $this->post_model->getPaginationPosts($limit, $start ,$_SESSION['blogFor']);
		}	

		$output = json_encode($postData);
		echo $output;
	}	

	public function pagination()
	{		
		$current = $_POST['current'];
		$limit = $_POST['limit'];
		$count = $_POST['count'];

		$paginationData['buttons'] = $this->common_model->pagination($count, $limit, $current);
		$paginationData['count'] = $count;
		$paginationData['limit'] = $limit;

		$output = json_encode($paginationData);
		echo $output;
	}

	public function viewPost($id)
	{  
		$postData = $this->post_model->getPostById($id);
		$postData->tags = $this->tag_model->getTagOfPost($id);
		
		$data = $this->common_model->loadTemplateData($postData->title); 
		$data['postData'] = $postData;
		
		$this->load->view('components/sections/singleNoSidebar', $data);
	}
	  
	public function category()
	{  
		$categories = $this->common_model->getAllData('categories');
		
		foreach($categories as $category){
			$category->count = $this->post_model->getCategoryPostsCount($category->id, $_SESSION['blogFor']);
		};

		$data=['pageName'=>"Categories",
				'categories' => $categories
			  ];
		
		$this->load->view('components/sections/categories', $data);
	}

	public function categoryPosts($input)
	{  
		$input=urldecode($input);
		$category = $this->common_model->getByFeild('categories', 'category', $input);		
		$postData = $this->post_model->getCategoryPosts($category[0]->id, $_SESSION['blogFor']);

		$data = $this->common_model->loadTemplateData($input); 
		$data['postData'] = $postData;

		$this->load->view('components/sections/listPosts', $data);
	}

	public function tagPosts($input)
	{  
		$input=urldecode($input);
		$tag = $this->common_model->getByFeild('tags', 'tagName', $input);		
		$postData = $this->post_model->getTagPosts($tag[0]->id, $_SESSION['blogFor']);

		$data = $this->common_model->loadTemplateData($input); 
		$data['postData'] = $postData;

		$this->load->view('components/sections/listPosts', $data);
	}

	public function authors()
	{  
		$authors = $this->common_model->getAllData('admin_users');
		
		foreach($authors as $author){
			$author->count = $this->post_model->getAuthorPostsCount($author->fullName, $_SESSION['blogFor']);
		};

		$data=['pageName'=>"Authors",
				'authors' => $authors
			  ];
		
		$this->load->view('components/sections/authors', $data);
	}
	
	public function authorPosts($input)
	{  
		$input=urldecode($input);
		
		$postData = $this->post_model->getAuthorPosts($input, $_SESSION['blogFor']);
		foreach($postData as $post){			
			$cat = $this->category_model->getCategory($post->category);
			$post->categoryName = $cat->categoryName;
		}

		$data = $this->common_model->loadTemplateData($input); 
		$data['postData'] = $postData;

		$this->load->view('components/sections/listPosts', $data);
	}

	public function months()
	{  
		//$months = $this->db->query("SELECT DISTINCT (DATE_FORMAT(date,'%Y-%m')) as month FROM posts")->result();
		$months = $this->month_model->getMonths($_SESSION['pagename']);
		$monthData=[];
		$i=0;
		foreach($months as $month){
			$monthData[$i]['month']=$month->month;
			$monthData[$i]['count']=$this->common_model->countCondition('posts', "date LIKE '%".$month->month."%' AND blogFor LIKE '%".$_SESSION['blogFor']."%' ");
			$i++;
		}

		$data=['pageName'=>"Months",
			   'monthData' => $monthData
	 		  ];

		 $this->load->view('components/sections/months', $data);
	}

	public function monthPosts($month)
	{  
		$postData = $this->common_model->getByCondition('posts', "date LIKE '%".$month."%' AND blogFor LIKE '%".$_SESSION['blogFor']."%' ");

		$data = [ 'pageName' => $month,
				  'postData' => $postData
			    ];
		
		$this->load->view('components/sections/singleMonth', $data);
	}

	public function search()
	{
		unset($_SESSION['searchResults']);

		$keyword = $_GET['keyword'];

		$searchResults = [];
		$firstResults  = [];
	
		$data['post_results']   = $this->post_model->searchPost($keyword,$_SESSION['blogFor']);
		
		foreach($data['post_results'] as $result):
			array_push($searchResults,$result->id);
		endforeach; 

		$data['category_results'] = $this->category_model->searchCategory($keyword,$_SESSION['blogFor']);
		if($data['category_results'] != null){
		foreach($data['category_results'] as $result):
			if (!in_array($result->id, $searchResults)){
				array_push($searchResults,$result->id);
			}
		endforeach;  
		}

		$data['tag_results']      = $this->tag_model->searchTag($keyword,$_SESSION['blogFor']);
		if($data['tag_results'] != null){
		foreach($data['tag_results'] as $result):
			if (!in_array($result->id, $searchResults)){
				array_push($searchResults,$result->id);
			}
		endforeach; 	
		}	
		
		$i = 0;
		while($i<count($searchResults)){
			$searchResults[$i] = $this->post_model->searchResult($searchResults[$i]);
			$i++;
		}

		$_SESSION['searchResults'] = $searchResults;
		$data['count']   = count($searchResults) ;
		if ($data['count']<12){
			$limit = $data['count'];
		}else{
			$limit = 12;
		}

		$paginationData['buttons'] = $this->common_model->pagination($data['count'], $limit, 1);
		$paginationData['count'] = $data['count'];
		$paginationData['limit'] = $limit;
		
		if(count($_SESSION['searchResults']) == 1 ){
			$firstResults[0] = ($_SESSION['searchResults'][0]);
		}else if(!empty($searchResults)){
			for($i=0;$i<$limit;$i++){
				$firstResults[$i] = ($_SESSION['searchResults'][$i]);
			}
		}

		$data=['postData' => $firstResults,
			   'pageName' => "Search",
			   'keyword'  => $keyword,
			   'count'    =>$data['count'],
			   'paginationData' => $paginationData];
		
		$this->load->view('components/sections/searchResults', $data); 
		//$this->load->view('search', $data); 
	}

	public function results()
	{		
		$id = $_POST['id'];

		// echo $id;

		$limit = 3;
		$start = ($id*$limit) - ($limit);
		$end   = $start+$limit;

		$searchResults = [];

		if($start != (count($_SESSION['searchResults'])-1)){
			for($i=$start;$i<$end;$i++){
				$searchResults[$i] = ($_SESSION['searchResults'][$i]);
			}
		}else{
			$searchResults[$start] = ($_SESSION['searchResults'][$start]);
		}
	
		$output = json_encode($searchResults);
		echo $output;

		// foreach($searchResults as $result){
		// 	echo $result->title.'<br>';
		// }
	}	

}